# Me2Balliance - Hugo Theme
A "Hugo - Static Site Generator" theme used in Me2Balliance

## Usage
Clone this repository into the themes/ folder. If you want you can rename it to something of your choosing, then modify the config.toml file with the following line:

theme = "me2balliance-theme" 
